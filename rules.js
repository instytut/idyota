var endRuleSet = [
    {inp:'jem',   out:'im',   post:'i',  comment:' reguła: -jem',    family:'[emRule]', stat:0},
    {inp:'kiem',  out:'kim',  post:'i',  comment:' reguła: -kiem',   family:'[emRule]', stat:0},
    {inp:'giem',  out:'gim',  post:'i',  comment:' reguła: -giem',   family:'[emRule]', stat:0},
    {inp:'em',    out:'ym',   post:'i',  comment:' reguła: -em',     family:'[emRule]', stat:0},
    {inp:'ém',    out:'ym',   post:'i',  comment:' reguła: -ém',     family:'[emRule]', stat:0},
	
    {inp:'ji',    out:'ii',   post:'',   comment:' reguła: -ji',     family:'[jotaRule]', stat:0},
    {inp:'ja',    out:'ia',   post:'ch', comment:' reguła: -ja',     family:'[jotaRule]', stat:0},
    {inp:'ją',    out:'ią',   post:'',   comment:' reguła: -ją',     family:'[jotaRule]', stat:0},
    {inp:'ję',    out:'ię',   post:'',   comment:' reguła: -ję',     family:'[jotaRule]', stat:0},
    {inp:'jami',  out:'iami', post:'',   comment:' reguła: -jami',   family:'[jotaRule]', stat:0},
    {inp:'jo',    out:'io',   post:'m',  comment:' reguła: -jo',     family:'[jotaRule]', stat:0},
    {inp:'je',    out:'ie',   post:'',   comment:' reguła: -je',     family:'[jotaRule]', stat:0},

    {inp:'jum',   out:'ium',  post:'',   comment:' reguła: -jum',    family:'[jotaRule]', stat:0},
    {inp:'jów',   out:'iów',  post:'',   comment:' reguła: -jów',    family:'[jotaRule]', stat:0},
    {inp:'ya',    out:'ja',   post:'ch', comment:' reguła: -ya>ja',  family:'[jotaRule]', stat:0},
    {inp:'yi',    out:'ji',   post:'',   comment:' reguła: -yi>ji',  family:'[jotaRule]', stat:0},
    {inp:'ye',    out:'je',   post:'',   comment:' reguła: -ye>je',  family:'[jotaRule]', stat:0},
    {inp:'yę',    out:'ję',   post:'',   comment:' reguła: -yę>ję',  family:'[jotaRule]', stat:0},
    {inp:'yą',    out:'ją',   post:'',   comment:' reguła: -yą>ją',  family:'[jotaRule]', stat:0},
    {inp:'yo',    out:'jo',   post:'m',  comment:' reguła: -yo>jo',  family:'[jotaRule]', stat:0},

    {inp:'ya',    out:'ia',   post:'ch', comment:' reguła: -ya>ia',  family:'[jotaRule]', stat:0},
    {inp:'yi',    out:'ii',   post:'',   comment:' reguła: -yi>ii',  family:'[jotaRule]', stat:0},
    {inp:'ye',    out:'ie',   post:'',   comment:' reguła: -ye>ie',  family:'[jotaRule]', stat:0},
    {inp:'yę',    out:'ię',   post:'',   comment:' reguła: -yę>ię',  family:'[jotaRule]', stat:0},
    {inp:'yą',    out:'ią',   post:'',   comment:' reguła: -yą>ią',  family:'[jotaRule]', stat:0},
    {inp:'yo',    out:'io',   post:'m',  comment:' reguła: -yo>io',  family:'[jotaRule]', stat:0},

    {inp:'eji',    out:'ei',   post:'',  comment:' reguła: -eji>ei',  family:'[jotaRule]', stat:0},
    {inp:'uji',    out:'ui',   post:'',  comment:' reguła: -uji>ui',  family:'[jotaRule]', stat:0},
    {inp:'oji',    out:'oi',   post:'',  comment:' reguła: -oji>oi',  family:'[jotaRule]', stat:0},
	
	// TODO: zasada dla "jakiem" na początku słowa
 	
    {inp:'irn',   out:'im',   post:'i',  comment:' reguła: ocr-irn', family:'[ocr]', stat:0},	
    {inp:'icli',  out:'ich',  post:'',   comment:' reguła: ocr-cli', family:'[ocr]', stat:0},	
];

var midRuleSet = [
    {inp:'é',     out:'e',    post:'',   comment:' reguła: -é > e',  family:'[eAcc]', stat:0},
    {inp:'onz',   out:'ąz',   post:'',   comment:' reguła: -onz-',   family:'[nasal]', stat:0},
    {inp:'ons',   out:'ąs',   post:'',   comment:' reguła: -ons-',   family:'[nasal]', stat:0},

	// below rules would work only in presence of allwords dictionary
	// otherwise "osadzę" is changed into "osadzą"
	// propose correction instead of changing

	// {inp:'ą',     out:'ę',    post:'',   comment:' reguła: ąę',      family:'[ocr]', stat:0},
    // {inp:'ę',     out:'ą',    post:'',   comment:' reguła: ęą',      family:'[ocr]', stat:0},
];