// requires util.js
// requires dictionary.js
// requires rules.js
// requires data.js for 
// -the list of letters
// -the default list of dictionaries in use

function changeWording(id) {

    var el = getNamedElement(id);
    var tmp = el.innerHTML;
    el.innerHTML = el.title;
    el.title = tmp;
}

function Spellchecker(name, debug) {

    this.missed = [];
    this.autotune = '';
    this.logStr = '';
    this.levenStr = '';
    this.txtHTML = '';
    this.txtRAW = '';
    this.knownCnt = 0;
    this.failCnt = 0;
    this.fixCnt = 0;
    this.rulesCnt = 0;
    this.guessCnt = 0;
    this.spanCnt = 0;
    this.hypoCnt = 0;
    this.autoFix = true;
    this.emRule = true;
    this.eAccRule = true;
    this.freqWords = [];
    this.freqScore = [];
	this.fixLineBreaks = false;
	this.isHTMLreturned = false;
  
    // Init dictionaries
    // variable order:
    // 1) dictionary name
    // 2) do we check for capitalization variants?
    // 3) explanation
  
  this.conDict = new Dictionary('conDict', true, ' (łączenie)');
  this.conDict.feedArray(connect_array);
  
  this.doubtDict = new Dictionary('doubtDict', true, ' (wątpliwe)');
  this.doubtDict.feedArray(doubt_array);
  
  // tables of dictionaries
  
  this.dictSet = [];
  this.dictTrigger = [];
  this.dictUsesFullWords = [];
  this.dictNo = 0;
  
  this.dictSet[this.dictNo] = new Dictionary('ocrDict', true, ' (ocr)');
  this.setDictParams(this.dictNo, ocr_array, false, '[ocr]');
 
  this.dictSet[this.dictNo] = new Dictionary('consDict', true, ' (dźwięczność)');
  this.setDictParams(this.dictNo, cons_array, false, '[dźwięczność]');
  
  this.dictSet[this.dictNo] = new Dictionary('divDict', true, ' (podział)');
  this.setDictParams(this.dictNo, divide_array, true, '[podział]');
  
  this.dictSet[this.dictNo] = new Dictionary('emDict', true, ' (-em/-emi)');
  this.setDictParams(this.dictNo, emEndings, true, '[em]'); 

  this.dictSet[this.dictNo] = new Dictionary('jotaDict', true, ' (jota)');
  this.setDictParams(this.dictNo, jota_array, true, '[jota]'); 

  this.dictSet[this.dictNo] = new Dictionary('jotaPartDict', true, ' (jota-fragment)');
  this.setDictParams(this.dictNo, partial_jota_array, false, '[jota]'); 
  
  this.dictSet[this.dictNo] = new Dictionary('mieDict', true, ' (mię)');
  this.setDictParams(this.dictNo, mie_array, true, '[mię]');
  
  this.dictSet[this.dictNo] = new Dictionary('blpDict', true, ' (biernik na ę)');
  this.setDictParams(this.dictNo, blp_array, true, '[blp]');
  
  this.dictSet[this.dictNo] = new Dictionary('dlmDict', true, ' (dopełniacz lm)');
  this.setDictParams(this.dictNo, dlm_arr, true, '[dlm]');
  
  this.dictSet[this.dictNo] = new Dictionary('dzDict', true, ' (bezokolicznik na dz)');
  this.setDictParams(this.dictNo, dz_verbs, true, '[dz]');
  
  this.dictSet[this.dictNo] = new Dictionary('countDict', true, ' (liczby)');
  this.setDictParams(this.dictNo, count_array, false, '[num]');
 
  this.dictSet[this.dictNo] = new Dictionary('typosDict', true, ' (literówki)');
  this.setDictParams(this.dictNo, typos_array, true, '[typo]');
   
  // should we automatically apply changes 
  // based  on  edit distance if they  get 
  // sufficiently good score?
  
  this.autoFix = true; 
}

Spellchecker.prototype = {
  
  setDictParams: function (num, arr, full, trigger) {

    this.dictSet[num].feedArray(arr);
    this.dictUsesFullWords[num] = full;
    this.dictTrigger.push(trigger);
    this.dictNo++;
  },
  
  getFreq: function (word) {
  
    const ln = this.freqWords.length;
  
    for (var i = 0; i < ln; i++) {
         if (word == this.freqWords[i]) 
         return this.freqScore[i];
    }
 
    return 0;
  },

  addFreq: function (word) {
  
    if (word == null || word == '' || word == ' ') return;
    if (!wordIsHint(word)) return;
     
    var ln = this.freqWords.length;
    word = word.toLowerCase();

    for (var i = 0; i < ln; i++) {
        if (this.freqWords[i] == word) {
            this.freqScore[i]++;
            return;
        }
    }

    this.freqWords.push(word);
    this.freqScore.push(1);
  },
  
  buildFreqDict: function(txt) {
 
    var tokens = txt.split(" ");
    var ln = tokens.length;

    // fill dictionary with frequencies

    for (var i = 0; i < ln; i++) {

        if (tokens[i].length > 2) {

            var sp = {prefix:'', curr:tokens[i], postfix:''};
            this.prefixPostfix(sp);

            if (sp.curr.length > 2) 
               this.addFreq(sp.curr);
        } 
    }

    //alert(this.freqWords.length);
  },
  
  getFreqDict: function() {
  
    this.findHTMLentities(); // TODO: only if script is linked to a HTML page
    this.activateDictionaries(); // ditto
    var txt = this.input.value;
    txt = this.Preprocess(txt);
    txt = this.Cleanup(txt);
    this.buildFreqDict(txt);
    var ln = this.freqWords.length;

    var dictArray = [];

    for (var i = 0; i < ln; i++) {
        locStr = '"'+this.freqWords[i] + ' ' + this.freqScore[i]+'",\n';
        dictArray.push(locStr);
    }

    dictArray.sort();

    var outStr = '';

    for (i = 0; i < ln; i++)
        outStr += dictArray[i];

    download("freq.txt", outStr);

  },

  onNewCorrection: function() {

    this.missed = [];
    this.autotune = '';
  
    this.logStr = '';
    this.levenStr = '';
    this.txtHTML = '';
    this.txtRAW = '';
    this.knownCnt = 0;
    this.failCnt = 0;
    this.fixCnt = 0;
    this.hypoCnt = 0;
    this.rulesCnt = 0;
    this.guessCnt = 0;
    this.spanCnt = 0;

    // clear rules statitics

    const endRulesLn = endRuleSet.length;
    for (l = 0; l < endRulesLn; l++)
        endRuleSet[l].stat = 0;

    const midRulesLn = midRuleSet.length;
    for (l = 0; l < midRulesLn; l++)
        midRuleSet[l].stat = 0;
  
    // clear dictionaries statistics

    for ( i = 0; i < this.dictNo; i++)
        this.dictSet[i].clearStats();
  
    this.conDict.clearStats();
    this.doubtDict.clearStats();
  },
  
  findHTMLentities: function() {

    this.input = getNamedElement('input');
    this.statOutput = getNamedElement('stat');
    this.timeInfo = getNamedElement('time');
    this.suggestOutput  = getNamedElement('suggest');
    this.unknownsOutput  = getNamedElement('unknowns');
    this.levenOutput = getNamedElement('leven');
    this.detailsOutput = getNamedElement('details');
    this.plainOutput = getNamedElement('plainOutput');
    this.htmlOutput  = getNamedElement('htmlOutput');
    this.dictList = getNamedElement('dictList');
  },
  
  allDictOn: function() {

    this.dictList.value = allDictionaries;  
  },

  activateDictionaries: function() {

    const s = this.dictList.value;
	
	if (s.contains('[fixLines]') ) this.fixLineBreaks = true;
	else                           this.fixLineBreaks = false;

    if (s.contains('[emRule]') ) this.emRule = true;
    else                         this.emRule = false;
 
    if (s.contains('[e]')   ) this.eAccRule = true;
    else                      this.eAccRule = false;
 
    if (s.contains('[łączenie]') ) this.conDict.active = true;
    else                          this.conDict.active = false;

    if (s.contains('[auto]') ) this.autoFix = true;
    else                       this.autoFix = false;

    for (i = 0; i < this.dictNo; i++) {

       if (s.contains(this.dictTrigger[i]) ) 
            this.dictSet[i].active = true;
        else                       
            this.dictSet[i].active = false;
    }

  },

  Preprocess: function(txt) {
	  
    // Formatowanie WL-XML

    // TODO: find UTF-8 code for the first space

    txt = txt.repAll(" ", " "); 
    txt = txt.repAll(">", "&gt; ");
    txt = txt.repAll("<", " &lt;");	
    txt = txt.repAll("\n", " [NEWLINE] ");
    txt = txt.repAll("[NEWLINE] ?", '[NEWLINE] ');
	
    return txt;
  },
  
  Postprocess: function(txt) {
    
    txt = txt.repAll("&gt; ", "&gt;");
    txt = txt.repAll(" &lt;", "&lt;");
	
	// opening tags need a space in front of them
	
	txt = txt.repAll("&lt;slowo_obce&gt;", " &lt;slowo_obce&gt;");
	txt = txt.repAll("&lt;wyroznienie&gt;", " &lt;wyroznienie&gt;");
	txt = txt.repAll("&lt;tytul_dziela&gt;", " &lt;tytul_dziela&gt;");
	txt = txt.repAll("&lt;osoba&gt;", " &lt;osoba&gt;");
	
	// closing tags need a space after them
	
	txt = txt.repAll("&lt;/slowo_obce&gt;", "&lt;/slowo_obce&gt; ");
	txt = txt.repAll("&lt;/wyroznienie&gt;", "&lt;/wyroznienie&gt; ");
	txt = txt.repAll("&lt;/tytul_dziela&gt;", "&lt;/tytul_dziela&gt; ");
	txt = txt.repAll("&lt;/osoba&gt;", "&lt;/osoba&gt; ");
	txt = txt.repAll("&lt;/motyw&gt;", "&lt;/motyw&gt; ");
	
	// punctuation marks and spaces
	
	txt = txt.repAll(" , ", ", ");
	txt = txt.repAll(" .", ".");
	txt = txt.repAll(" !", "!");
	txt = txt.repAll(" ?", "?");
	txt = txt.repAll(" )", ")");
	txt = txt.repAll(" :", ":");
	txt = txt.repAll(" ;", ";");

	txt = txt.repAll("---!", "--- !");
	txt = txt.repAll("---?", "--- ?");
	
	// spaces
	
	// usuwanie nadmiarowych spacji: <pe> <slowo_obce>
	txt = txt.repAll("&lt;pe&gt; &lt;slowo_obce&gt;", "&lt;pe&gt;&lt;slowo_obce&gt;"); 
	
	// usuwanie nadmiarowych spacji: przypis do słowa obcego
	txt = txt.repAll("&lt;/slowo_obce&gt; &lt;pe&gt;", "&lt;/slowo_obce&gt;&lt;pe&gt;"); 
	
	// usuwanie nadmiarowych spacji: przypis do tytułu
	txt = txt.repAll("&lt;/tytul_dziela&gt; &lt;pe&gt;", "&lt;/tytul_dziela&gt;&lt;pe&gt;");
	
	// usuwanie nadmiarowych spacji: przypis do wyróżnienia
	txt = txt.repAll("&lt;/wyroznienie&gt; &lt;pe&gt;", "&lt;/wyroznienie&gt;&lt;pe&gt;");
	
	txt = txt.repAll("&lt;/pe&gt;", "&lt;/pe&gt; ");
	
	txt = txt.repAll(" &lt;/motyw&gt;", "&lt;/motyw&gt;");
	
	txt = txt.repAll("( ", "(");
	txt = txt.repAll(" )", ")");
	
	// punctuation marks after footnotes
	 
	txt = txt.repAll("&lt;/pe&gt; ,", "&lt;/pe&gt;,"); 
	txt = txt.repAll("&lt;/pe&gt; .", "&lt;/pe&gt;.");
	txt = txt.repAll("&lt;/pe&gt; ?", "&lt;/pe&gt;?");
	txt = txt.repAll("&lt;/pe&gt; !", "&lt;/pe&gt;!");
	txt = txt.repAll("&lt;/pe&gt; :", "&lt;/pe&gt;:");
	txt = txt.repAll("&lt;/pe&gt; ;", "&lt;/pe&gt;;");
	
	txt = txt.repAll("&lt;/pr&gt; ,", "&lt;/pr&gt;,"); 
	txt = txt.repAll("&lt;/pr&gt; .", "&lt;/pr&gt;.");
	txt = txt.repAll("&lt;/pr&gt; ?", "&lt;/pr&gt;?");
	txt = txt.repAll("&lt;/pr&gt; !", "&lt;/pr&gt;!");
	txt = txt.repAll("&lt;/pr&gt; :", "&lt;/pr&gt;:");
	txt = txt.repAll("&lt;/pr&gt; ;", "&lt;/pr&gt;;");
	
	txt = txt.repAll("&lt;/pa&gt; ,", "&lt;/pa&gt;,"); 
	txt = txt.repAll("&lt;/pa&gt; .", "&lt;/pa&gt;.");
	txt = txt.repAll("&lt;/pa&gt; ?", "&lt;/pa&gt;?");
	txt = txt.repAll("&lt;/pa&gt; !", "&lt;/pa&gt;!");
	txt = txt.repAll("&lt;/pa&gt; :", "&lt;/pa&gt;:");
	txt = txt.repAll("&lt;/pa&gt; ;", "&lt;/pa&gt;;");
	
	txt = txt.repAll("  ", " ");

    return txt;
  },

  Cleanup: function(txt) {

    // remove double spaces
    // TODO: use regex

    txt = txt.repAll("  ", " ");

    return txt;
  },

  plainToWindow: function(txt) {

    txt = txt.repAll("[NEWLINE] ", "\n");
    txt = txt.repAll(" \n", "\n");
    return txt;
  },

  plainToHTML: function(txt) {
	
	if (this.fixLineBreaks) {
		
       txt = txt.repAll("[NEWLINE] ", " ");
       txt = txt.repAll("  ", " ");
	   txt = txt.repAll("&lt;akap", "<br><br>&lt;akap");                     // <akap>, <akap_dialog>  
	   txt = txt.repAll("&lt;naglo", "<br><br>&lt;naglo");                   // <naglowek_rozdzial>  
	   txt = txt.repAll("&lt;sekcja_swiatlo", "<br><br>&lt;sekcja_swiatlo"); // <sekcja_swiatlo/>
		
	} else {
       txt = txt.repAll("[NEWLINE] ", "<br>");
       txt = txt.repAll(" <br>", "<br>");
	}
	
    return txt;
  },
  
  fixSpaces: function(txt) {
	
	txt = txt.repAll(" &lt;/motyw", "&lt;/motyw");

    return txt;	
  },
  
  wordSpan: function(word, comment, _class) {

    this.spanCnt++;
    var id = 'no'+this.spanCnt;
    var idStr = ' id="' + id + '"';
    var internalStr = "'" + id + "'";
    var functStr = ' onclick="changeWording('
                 + internalStr
                 +')"'
				 
    var htmlSpanStart = " ";
	var htmlSpanEnd = " ";			 
	
    if (this.isHTMLreturned) {	
	    htmlSpanStart = " &lt;span class='corrected'&gt;";
	    htmlSpanEnd = "&lt;/span&gt; ";
	
	    if (_class == "error") {
	        htmlSpanStart = " &lt;span class='error'&gt;";
	    }
	}

    return htmlSpanStart+'<b><span'   +  idStr
           + ' class="' + _class  + '"'
           + ' title="' + comment + '"'
           +   functStr + '>'
           + word + '</span></b>' + htmlSpanEnd; 
  },
  
  logSpan: function(tuple, _class) {
 
    var id = 'no'+this.spanCnt;
    var linkStr = '<a class="hiddenLink" href="#' + id + '">';

    return   tuple.inp + linkStr + ' > <b><span class="' + _class +'">'
           + tuple.out + '</span></b> '+ '</a>' + tuple.comment;  
  },
  
  processConnectedWord: function(prefix, word, postfix, oldForm) {

    var newForm = prefix + word + postfix;
    this.knownCnt++;
    this.txtHTML += (this.wordSpan(newForm, oldForm, 'dict') + " ");
    this.txtRAW = this.txtRAW + newForm + " ";
   
    // TODO: real link to the place where the words have been connected
    var tuple = {inp:oldForm, out:newForm, comment:"(łączenie)"};
    this.logStr += 'Poprawka: ' + this.logSpan(tuple, 'połączenie') + '<br>';

  },

  processKnownWord: function(prefix, word, postfix) {

    oldForm = prefix + word + postfix;
    this.knownCnt++;
    var tuple = {inp:word, out:word, comment:''};

    this.doubtDict.callMe(tuple);
    if (tuple.inp != tuple.out) {
        var newForm = prefix + tuple.out + postfix;
        this.txtHTML += (this.wordSpan(oldForm, newForm,'doubt') + " ");
        this.logStr += 'Wątpliwość: ' + this.logSpan(tuple, 'doubt') + '<br>';
    /*
    } else if (tuple.inp.charAt(0) == 'n') {
		var ln = tuple.out.length;
		tuple.out = 'm' + tuple.out.slice(1,ln);
		if (wordIsKnown(tuple.out)) {
	        var newForm = prefix + tuple.out + postfix;
            this.txtHTML += ('<h4>'+this.wordSpan(oldForm, newForm,'doubt') + " </h4>");
            this.logStr += 'Wątpliwość: ' + this.logSpan(tuple, 'doubt') + '<br>';
		}
        else this.txtHTML += (oldForm + " ");
    */  
	} else {
        this.txtHTML += (oldForm + " ");
    }
    this.txtRAW = this.txtRAW + oldForm + " ";
  },
  
processSingleEnding: function(tuple, oldEnd, newEnd, comment, rule) {

    if (tuple.inp == tuple.out) {

        var ln = tuple.inp.length;
        var lnOld = oldEnd.length;
        var lnNew = newEnd.length;

        if (tuple.inp.slice(-lnOld) == oldEnd) {
            var root = tuple.inp.slice(0,ln-lnOld);
            var fix = root + newEnd;
            if (wordIsKnown(fix)) {
                tuple.out = fix;
                tuple.comment = comment;
                this.rulesCnt++;
                rule.stat++;
            }
        }
    }
  },
  
  processMidRule: function(tuple, rule, on) {
  
    if (on && tuple.inp == tuple.out) {

        if (tuple.out.contains(rule.inp) ) {
            var fix = tuple.out.repAll(rule.inp, rule.out);
            if (wordIsKnown(fix)) {
               tuple.out = fix;
               tuple.comment = rule.comment;
               rule.stat++;
               this.rulesCnt++;
            } 
        }  
    }  
  },
  
  processEndRule: function(tuple, rule, on) {
  
    if (on && tuple.inp == tuple.out) {

        this.processSingleEnding(tuple, rule.inp, rule.out, rule.comment, rule);
        if (rule.post != '')
           this.processSingleEnding(tuple, rule.inp + rule.post, rule.out + rule.post, rule.comment, rule);

    }  
  },

  processUnknownWord: function(prefix, word, postfix) {

    var tuple = {inp:word, out:word, comment:''};
    var levensteined = false;
    var hypothesis = false;
    
    // if our word is not on dynamically updated list
    // of failed corrections, try to fix it 
    
    if (!this.missed.includes(word)) {

        // correction by dictionaries

        for (i = 0; i < this.dictNo; i++) {
            if (this.dictUsesFullWords[i] == true)
                this.dictSet[i].callMe(tuple);
            else
                this.dictSet[i].callPart(tuple);
        }

        // corrections by rules

        const endRulesLn = endRuleSet.length;
        for (l = 0; l < endRulesLn; l++)
            this.processEndRule(tuple, endRuleSet[l], this.emRule);

        const midRulesLn = midRuleSet.length;
        for (l = 0; l < midRulesLn; l++)
            this.processMidRule(tuple, midRuleSet[l], this.eAccRule);

        // corrections by edit distance
    
        if (tuple.inp == tuple.out
        &&  tuple.inp.length > 2
        &&  this.autoFix) {
            var edit = edit1(tuple.inp);
            var editLn = edit.length;
            var editOptions = [];
            var editScores = [];

            for (var k = 0; k < editLn; k++) {
                if (wordIsHint(edit[k])) {
                    editOptions.push(edit[k]);
                    editScores.push(this.scoreSuggestion(tuple.inp, edit[k]));
                    if (getLast(editScores) > 99) {
                        this.guessCnt++;
                        levensteined = true;
                        tuple.out = edit[k];
                        tuple.comment = ' (dystans edycyjny)';
                        break;
                    }
                }
            }

            if (editOptions.length > 0) {
                var bestScore = 0;
                var bestWord = '';
                var ln = editOptions.length;
                this.levenStr += wrapInBold(tuple.inp);

                for (var i = 0; i < ln; i++) {

                    var currScore = editScores[i] + 100 * this.getFreq(editOptions[i]);
                    if (currScore > bestScore) {
                        bestScore = currScore;
                        bestWord = editOptions[i];
                    }

                    this.levenStr += wrapInBrackets(editOptions[i]);
                }

                this.levenStr += wrapInBold(bestWord);
                this.levenStr += '<br>';
            }

            if (editOptions.length == 1
            &&  !isUpperCase(tuple.inp))  {
                this.autotune = this.autotune
                + '"' + tuple.inp + ' > ' + editOptions[0] + '",<br>';
                tuple.out = editOptions[0];
                levensteined = true;
                hypothesis = true;
                this.hypoCnt++;
                tuple.comment = ' (dystans edycyjny bez alternatyw)';
            }

            if (editOptions.length > 1
            &&  !isUpperCase(tuple.inp)
            &&  wordIsHint(bestWord))  {
               
                tuple.out = bestWord;
                levensteined = true;
                hypothesis = true;
                this.hypoCnt++;
                tuple.comment = ' (frekwencja)';
            }
        }
    }

    // recording results

    var oldForm = prefix + tuple.inp + postfix;
    var newForm = prefix + tuple.out + postfix;
  
    if (tuple.inp != tuple.out) { // we have changed the word
        
       if (tuple.comment.contains('reguła')) {
            this.txtHTML += (this.wordSpan(newForm, oldForm,'rule') + " ");
            this.logStr += 'Poprawka: ' + this.logSpan(tuple, 'rule') + '<br>';
        } else if (levensteined) {
            if (hypothesis) {
                this.txtHTML += (this.wordSpan(oldForm, newForm,'error') + " ");
                this.logStr += 'Hipoteza: ' + this.logSpan(tuple, 'error') + '<br>';
            } else {
                this.txtHTML += (this.wordSpan(newForm, oldForm,'auto') + " ");
                this.logStr += 'Poprawka: ' + this.logSpan(tuple, 'auto') + '<br>';
            }
        } else {
            this.txtHTML += (this.wordSpan(newForm, oldForm, 'dict') + " ");
            this.logStr += 'Poprawka: ' + this.logSpan(tuple, 'dict') + '<br>';
        }
    
    } else {  // we don't know a word and it hasn't been fixed
  
        if (!this.missed.includes(tuple.inp)) {
           this.missed.push(tuple.inp);
        }
 
        this.txtHTML += (spanUnknown(oldForm) + " ");
        // this.logStr += 'Nieznane słowo: ' + spanUnknown(tuple.inp) + '<br>';
        this.failCnt++;
    }

    this.txtRAW = this.txtRAW + newForm + " ";
  },
  
  calcFixed: function() {
    this.fixCnt = this.conDict.succ
                + this.guessCnt
                + this.rulesCnt
                + this.doubtDict.succ // not sure if it should be added
                ;

                for (i = 0; i < this.dictNo; i++)
                   this.fixCnt += this.dictSet[i].succ;
  },
  
  printStat: function() {
  
    this.calcFixed(); // count the corrections

    var wordCnt = this.knownCnt + this.failCnt + this.fixCnt;
    if (wordCnt < 1) wordCnt = 1;

    var knownRatio  = Math.floor(this.knownCnt  * 10000 / wordCnt);
    var failedRatio = Math.floor(this.failCnt * 10000 / wordCnt);
    var fixedRatio  = Math.floor(this.fixCnt  * 10000 / wordCnt);
    var efficiency = Math.floor(10000 * fixedRatio / (fixedRatio + failedRatio));

    var detailStr = '';
    var statStr = 'Słów: ' + wordCnt + '<br>'
                + 'Znanych: '      + this.knownCnt  + ' (' + knownRatio + ')<br>'
                + 'Nieznanych: '   + this.failCnt + ' (' + failedRatio+ ')<br>'
                + 'Poprawek: '     + this.fixCnt  + ' (' + fixedRatio + ')<br>'
                + 'Skuteczność: '  + efficiency + '<br>';
                + '<br>';

    if (this.fixCnt) {
        detailStr = statStr + 'W tym: <br>'
            + 'połączone: '     + this.conDict.succ + '<br>'
            + 'reguły: '        + this.rulesCnt + '<br>'
            + 'odgadnięte: '    + this.guessCnt + '<br>'
            + 'wątpliwości: '   + this.doubtDict.succ + '<br>'
            + 'hipotezy: '      + this.hypoCnt + '<br>'
            ;
 
            for ( i = 0; i < this.dictNo; i++) {
                detailStr += 'Słownik ' + this.dictSet[i].comment + ': ' 
                          + this.dictSet[i].succ + '<br>';
        }
    }

    const endRulesLn = endRuleSet.length;

    for (var l = 0; l < endRulesLn; l++)
        detailStr += endRuleSet[l].comment + ' ' + endRuleSet[l].stat + '<br>';

    const midRulesLn = midRuleSet.length;

    for (var l = 0; l < midRulesLn; l++)
        detailStr += midRuleSet[l].comment + ' ' + midRuleSet[l].stat + '<br>';

    this.statOutput.innerHTML = statStr;  
    this.detailsOutput.innerHTML = detailStr; 

  },
  
  prefixPostfix: function(sp) {

    // split prefix

    var first = sp.curr.slice(0,1);
    while (isPunctuation(first) && sp.curr.length > 1) {
        sp.prefix = sp.prefix + first;
        sp.curr = sp.curr.slice(1);
        first = sp.curr.slice(0,1);
    }

    // split postfix

    var last = sp.curr.slice(-1);
    while (isPunctuation(last) && sp.curr.length > 1) {
        sp.postfix = last+sp.postfix;
        sp.curr = sp.curr.slice(0,sp.curr.length-1);
        last = sp.curr.slice(-1);
    }
  },

  Fix: function(txt) {

    this.logStr = ' ';
    txt = this.Preprocess(txt);
    txt = this.Cleanup(txt);

    // tokenize

    var tokens = txt.split(" ");
    var ln = tokens.length;

    // Create frequency dictionary of the current text
    // in order to have hints a bit more in line with 
    // the current text. This might take some time.

    var boxId = getNamedElement('freq_box');
    if (boxId.checked)
       this.buildFreqDict(txt);
   
    // Decide whether we return HTML
	
	boxId = getNamedElement('html_box');
	if (boxId.checked)
		this.isHTMLreturned = true;

    // rebuild text

    for (var i = 0; i < ln; i++) {

        // try connecting words

        var hasBeenConnected = false;
        var oldForm = '';

        if (i < ln-1) {
            var sp = {prefix:'', curr:tokens[i] + ' ' + tokens[i+1], postfix:''};
            this.prefixPostfix(sp);

            var tuple = {inp:sp.curr, out:sp.curr, comment:''};
            this.conDict.callMe(tuple);
            if (tuple.inp != tuple.out) {
                tokens[i] = sp.prefix + tuple.out + sp.postfix;
                tokens[i+1] = '';
                hasBeenConnected = true;
                oldForm = tuple.inp;
            }
        }

        // safeguard against leading space

        if (tokens[i] == '') continue;

        // discover WL-XML tags; cut off leading 
        // and trailing punctuaton marks, henceforth
        // referred to as sp.prefix and sp.postfix

        var isTag = false;
        if (inDictionary(tokens[i], wl_array)) isTag = true;
        var sp = {prefix:'', curr:tokens[i], postfix:''};
        if (!isTag) this.prefixPostfix(sp);

        // correct the word and display information
        // about what we have done

        if (hasBeenConnected) {
           this.processConnectedWord(sp.prefix, sp.curr, sp.postfix, oldForm);
        } else {
           if ( this.fixLineBreaks || wordIsKnown(sp.curr) 
           || !isNaN(parseFloat(sp.curr))) 
              this.processKnownWord(sp.prefix, sp.curr, sp.postfix);
           else 
              this.processUnknownWord(sp.prefix, sp.curr, sp.postfix);
        }
    }

  },
  
  makeCorrection: function() {
  
    var d = new Date();
    var startTime = d.getTime();

    this.onNewCorrection();
    this.findHTMLentities(); // TODO: only if script is linked to a HTML page
    this.activateDictionaries(); // ditto
    var txt = this.input.value;

    this.Fix(txt); 

    this.missed.sort();
    var missedWords = '';
    var missedLn = this.missed.length;

    for (var i = 0; i < missedLn; i++)
        missedWords = missedWords + '"' + this.missed[i] + '",<br>';

    this.plainOutput.innerHTML = this.plainToWindow(this.txtRAW);
    this.txtHTML = this.plainToHTML(this.txtHTML);
    this.txtHTML = this.Postprocess(this.txtHTML);
	this.txtHTML = this.fixSpaces(this.txtHTML);
	this.htmlOutput.innerHTML = this.txtHTML;
    this.printStat();
    this.suggestOutput.innerHTML = this.logStr;
    this.unknownsOutput.innerHTML = missedWords;
    this.levenOutput.innerHTML = this.levenStr;

    var e = new Date();
    var endTime = e.getTime();
    var runtime = endTime - startTime;
    this.timeInfo.innerHTML = '<br> Czas korekty: ' + runtime + ' milisekund';
  },
  
  scoreSuggestion: function(w1, w2) {

    var result = 0;
    var ln = 0;
    
    //////////////////////////////////////////////
    // Here we try to decide which of possible  //
    // corrections is better. "Better" depends  //
    // on  global program settings - we  don't  //
    // want to promote corrections which aren't //
    // desired by the user.                     //
    //////////////////////////////////////////////

    const s = this.dictList.value;

    // record endings, as errors there 
    // are both common and recurring

    const w1_2 = w1.slice(-2);
    const w2_2 = w2.slice(-2);
    const w1_3 = w1.slice(-3);
    const w2_3 = w2.slice(-3);
    const w1_4 = w1.slice(-4);
    const w2_4 = w2.slice(-4);
    const w1_5 = w1.slice(-5);
    const w2_5 = w2.slice(-5);
    
    if (s.contains('[jota]')) {
    
             if (w1_5 == "logja" && w2_5 == "logia") return 100;
        else if (w1_5 == "logji" && w2_5 == "logii") return 100;
        else if (w1_5 == "logją" && w2_5 == "logią") return 100;
        else if (w1_5 == "logję" && w2_5 == "logię") return 100;

             if (w1_3 == "cya"   && w2_3 == "cja") result += 40;
        else if (w1_3 == "cyę"   && w2_3 == "cję") result += 40;
        else if (w1_3 == "cyą"   && w2_3 == "cją") result += 40;
        else if (w1_3 == "cyi"   && w2_3 == "cji") result += 40;
    }

    if (this.emRule) {
      
        if (w1_4 == "czem"  &&  w2_4 == "czym" ) return 100;
        if (w1_4 == "kiem"  &&  w2_3 == "kim"  ) return 100;
        if (w1_4 == "giem"  &&  w2_3 == "gim"  ) return 100;
        if (w1_4 == "niem"  &&  w2_3 == "nim"  ) return 100;
        if (w1_5 == "czemś" &&  w2_5 == "czymś") return 100;
        if (w1_4 == "jemi"  &&  w2_3 == "imi"  ) return 100;
        if (w1_4 == "kiem"  &&  w2_3 == "kim"  ) return 100;
        if (w1_5 == "kiemi" &&  w2_4 == "kimi" ) return 100;
        if (w1_3 == "jem"   &&  w2_2 == "im"   ) return 100;
      
             if (w1_3 == "cem" && w2_3 == "cym") result += 40;   
        else if (w1_3 == "wem" && w2_3 == "wym") result += 40; 
        else if (w1_3 == "tem" && w2_3 == "tym") result += 40;
        else if (w1_3 == "emi" && w2_3 == "ymi") result += 40;
        else if (w1_3 == "emi" && w2_3 == "imi") result += 40;
        else if (w1_3 == "nem" && w2_3 == "nym") result += 40; 
        else if (w1_3 == "łem" && w2_3 == "łym") result += 40; 
        else if (w1_3 == "mem" && w2_3 == "mym") result += 40;
    }
    
    if (w1.length == w2.length) result += 10;
    
    if (w1.length > w2.length) ln = w2.length;
    else ln = w1.length;
    
    for (var i = 0; i < ln; i++) {
        
       var char1 = w1.charAt(i);   
       var char2 = w2.charAt(i);
        
       if (char1 == "é") {
          if (char2 == "e") result += 40;
          if (char2 == "y") result += 20;
          if (char2 == "i") result += 20;
       } 

       switch (char1) {
         case "e": if (char2 == "y") result += 10; break;
         case "t": if (char2 == "ż") result += 10; break;
         case "c": if (char2 == "ć") result += 40; break;
         case "y": {
            if (char2 == "e") result += 10;
            else if (char2 == "i") result += 20; 
         } break;
         case "u": if (char2 == "ó") result += 10; break;
         case "ż": if (char2 == "t") result += 10; break;
         case "ć": if (char2 == "ó") result += 10; break;
         case "ó": if (char2 == "ć" || char2 == "u") result += 10; break;
       }
       
    }
    
    if (w1.charAt(0) == w2.charAt(0)) result += 30;
    if (w1.charAt(1) == w2.charAt(1)) result += 10;
    if (w1.slice(-1) == w2.slice(-1)) result += 10;
    
    return result;
  },

}

function isPunctuation(item) {
 
    return (punctuation.includes(item));
}

function edit1(word) {

    var proposals = [];
    var ln = word.length;
    var opt = letters.length;
    var pre, post, cur;
   
    for (i = 1; i <= ln; i++) {
        pre = word.slice(0,i);
        post = word.slice(i);
        cur = pre.slice(-1);
        pre = pre.slice(0,-1);
       
        // deletions
       
        if (cur != pre.slice(-1))
           proposals.push(pre+post);
       
        // insertions
       
        for (j = 0; j < opt; j++)
            proposals.push(pre+letters[j]+cur+post);    
           
        // replacements
    
        for (j = 0; j < opt; j++) 
            proposals.push(pre+letters[j]+post); 
   
        // transpositions
  
        prepre = pre.slice(-1);
        initial = pre.slice(0,-1);
        proposals.push(initial + cur + prepre + post);
   }
   
    // insertions of the last letter
   
    for (j = 0; j < opt; j++)
        proposals.push(word+letters[j]);    
   
    return proposals;
}

var spell = new Spellchecker('spell', false);
