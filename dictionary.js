// requires: sg_dict_manager.js for inDictionary() function

function Dictionary(name, canCapitalize, explanation) {

  this.one = [];          // forms that should be replaced
  this.two = [];          // their replacements
  this.detectables = [];  // same as "one", but sorted, for speedup
  this.sorted = false;
  this.active = true;
  this.ln = 0;
  this.canCapitalize = canCapitalize;
  this.succ = 0;
  this.comment = explanation;
  this.name = name;
}

Dictionary.prototype = {

  Add: function(_in, _out) {

    this.one.push(_in);
    this.two.push(_out);
    this.detectables.push(_in);
  },
  
  expandEnding(stem, endRule) {
  
    switch (endRule) {
        case undefined:
            alert('Błąd w słowniku ' + this.name + ': ' + stem);
        break;

        case "dz":
            this.Add(stem + 'dz' , stem + 'c');
        break;

        case "em":
            this.Add(stem + 'em' , stem + 'ym');
            this.Add(stem + 'emi', stem + 'ymi');
            this.Add(stem + 'ém' , stem + 'ym');
            this.Add(stem + 'émi', stem + 'ymi');
        break;

        case "iem":
            this.Add(stem + 'iem' , stem + 'im');
            this.Add(stem + 'iemi', stem + 'imi');
            this.Add(stem + 'iém' , stem + 'im');
            this.Add(stem + 'iémi', stem + 'imi');
        break;

        case "ja":
            this.Add(stem+'ja', stem + 'ia');
            this.Add(stem+'ją', stem + 'ią');
            this.Add(stem+'ję', stem + 'ię');
            this.Add(stem+'je', stem + 'ie');
            this.Add(stem+'ji', stem + 'ii');
            this.Add(stem+'jo', stem + 'io');
            this.Add(stem+'jom', stem + 'iom');
            this.Add(stem+'jami', stem + 'iami');
            this.Add(stem+'jach', stem + 'iach');
        break;

        case "jum":
            this.Add(stem+'jum', stem + 'ium'); 
            this.Add(stem+'jami', stem + 'iami'); 
            this.Add(stem+'jom', stem + 'iom');
            this.Add(stem+'jów', stem + 'iów'); 
            this.Add(stem+'jach', stem + 'iach'); 
            this.Add(stem+'ja', stem + 'ia');
        break;

        case "ya_ja":
            this.Add(stem+"ya", stem+"ja");
            this.Add(stem+"yi", stem+"ji");
            this.Add(stem+"ye", stem+"je");
            this.Add(stem+"yę", stem+"ję");
            this.Add(stem+"yach", stem+"jach");
            this.Add(stem+"yami", stem+"jami");
            this.Add(stem+"yom", stem+"jom");
            this.Add(stem+"yo", stem+"jo");
            this.Add(stem+"yą", stem+"ją");
        break;

        case "ya_ia": 
            this.Add(stem+"ya", stem+"ia");
            this.Add(stem+"yi", stem+"ii");
            this.Add(stem+"ye", stem+"ie");
            this.Add(stem+"yę", stem+"ię");
            this.Add(stem+"yach", stem+"iach");
            this.Add(stem+"yami", stem+"iami");
            this.Add(stem+"yom", stem+"iom");
            this.Add(stem+"yo", stem+"io");
            this.Add(stem+"yą", stem+"ią");
        break;
    }
  },
  
  expandStem: function(stemRule, endRule) {
  
    if (stemRule.contains('|')) {
      var stem = stemRule.split('|');
      var pos = stem.length-1;
      this.expandEnding(stem[pos], endRule);
  
      if (stem.length == 2 && stem[pos-1].contains('/')) {
          var variants = stem[pos-1].split('/');
          var varLn = variants.length;
          for (var j = 0; j < varLn; j++) {
             this.expandEnding(variants[j] + stem[pos], endRule);
          }
      } else {
          this.expandEnding(stem[pos-1]+stem[pos], endRule);
      }

      if (pos > 1) {
         if (stem[pos-2].contains('/')
         && stem[pos-1].contains('/')) {
		    var variants1 = stem[pos-1].split('/');
		    var variants2 = stem[pos-2].split('/');
		    var varLn1 = variants1.length;
		    var varLn2 = variants2.length;
		   
		    for (var j = 0; j < varLn1; j++) {
			    this.expandEnding(variants1[j] + stem[pos], endRule);
			    for (var k = 0; k < varLn2; k++) {
				    this.expandEnding(variants2[k] + variants1[j] + stem[pos], endRule);
			    }
		    }

        } else if (stem[pos-2].contains('/')) {

            var variants = stem[pos-2].split('/');
            this.expandEnding(stem[pos-1] + stem[pos], endRule);
            var varLn = variants.length;
            for (var j = 0; j < varLn; j++) {
                this.expandEnding(variants[j] + stem[pos-1] + stem[pos], endRule);
            }	

        } else if (stem[pos-1].contains('/')) {

            var variants = stem[pos-1].split('/');
            var varLn = variants.length;
            for (var j = 0; j < varLn; j++) {
                this.expandEnding(variants[j] + stem[pos], endRule);
                this.expandEnding(stem[pos-2] + variants[j] + stem[pos], endRule);
            }			 
			 
		} else {
            this.expandEnding(stem[pos-2]+stem[pos-1]+stem[pos], endRule);
		}
	  }
			
	} else {
          this.expandEnding(stemRule, endRule);
	}  
  },

  feedArray: function(arr) {
	
    var i = arr.length;
	
    while (i--) {
        if (arr[i].contains('>')) {
            var tokens = arr[i].split(" > ");
            this.Add(tokens[0], tokens[1]);
	    } else if (arr[i].contains('@')) {
            var tokens = arr[i].split(" @ ");
            var stemRule = tokens[0];
            var endRule = tokens[1];
            this.expandStem(stemRule, endRule); 
        }
    }

    this.detectables.sort();
    this.sorted = true;
  },

  getSize: function() {
    return this.one.length;
  },
  
  callMe: function(tuple) {

    if (tuple.inp == tuple.out) {
        tuple.out = this.Replace(tuple.inp);
        if (tuple.inp != tuple.out) tuple.comment = this.comment;
    } 
  },
  
  callPart: function(tuple) {

   if (tuple.inp == tuple.out) {
        tuple.out = this.replacePart(tuple.inp);
        if (tuple.inp != tuple.out
        &&  wordIsKnown(tuple.out)) {
           this.succ++;
           tuple.comment = this.comment;
      } else tuple.out = tuple.inp; // revert if change not confirmed by dictionary
    }

  },

  Replace: function(word) {
  
    if (!this.active) return word;
 
    // Speedup: check whether this word is replaceable
    // before looking for a replacement

    if (this.sorted) {

      if (!inDictionary(word, this.detectables))
         if (!inDictionary(uncapitalizeFirst(word), this.detectables)) 
         return word;
    }

    this.succ++;

    var i = this.one.length;
    while (i--) {
        if (word == this.one[i]) 
            return this.two[i];
        if (this.canCapitalize) {
            if (isSaneUpperCase(word)) {
                if (word == capitalizeFirst(this.one[i]))
                   return capitalizeFirst(this.two[i]);
            }
        }
    }

    this.succ--;
    return word;
  },
  
replacePart: function(word) {

    if (!this.active) return word;

    ln = this.one.length;
   
    while (ln--) {
        if (word.contains(this.one[ln]) ) {
          word = word.repAll(this.one[ln], this.two[ln]);
          return word;
        }
        if (word.contains(capitalizeFirst(this.one[ln])) ) {
            word = word.repAll(capitalizeFirst(this.one[ln]), capitalizeFirst(this.two[ln]));
            return word;
        }  
   
    }

    return word;
  },
  
  clearStats: function() {
    this.succ = 0;
  }

}