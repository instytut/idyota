// lista słowników używanych przez program
 
const allDictionaries = 
'[ocr][em][emRule][e][mię][jota][podział][łączenie][dźwięczność][dz][num][blp][dlm][typo][auto]';

// litery do wykorzystania przy generowaniu słów
// (patrz: dystans edycyjny)

const letters = [
    'a','ą','b','c','ć','d','e','ę','f','g','h','i','j','k','l','ł',
    'm','n','o','ó','p','r','s','ś','t','u','w','y','z','ż','ź'
];
	
// znaki interpunkcyjne (nie podlegają korekcie,
// jeśli znajdują się na początku/na końcu słowa)

const punctuation = [
    ',', '.', '"', "'", '„', "“", '”', '«', "»", "	", 
    '(', ')', '!', '?', '…', "*", ';', ":", "/",
];

// słowa, które mogą być błędnie połączone dywizem
// z innymi słowami

var dontSplitOnHyphen = [
"że",
"tam",
"by",
"co",
"bądź",
"tam",
"jakiejś",
"jakiej",
"na",
"niby",
"po",
"pół",
"sam",
"saint",
"indziej",
"xxx > xxx"
];

// jednolementowy pseudosłownik do zamiany "mię" na "mnie"

var mie_array = [
"mię > mnie",
"xxx > xxx"
];

// poprawiane formy dopełniacza liczby mnogiej

var dlm_arr = [
"wzgórzy > wzgórz", 
"wnętrzy > wnętrz", 
"xxx > xxx"
];

// poprawiane formy biernika liczby pojedynczej

blp_array = [
"jednę > jedną",
"królowę > królową",
"całę > całą",
"moję > moją",
"niejednę > niejedną",
"służącę > służącą",
"biednę > biedną",
"xxx > xxx"
];

// poprawianie bezokolicznika na -dz
// (z mnóstwem prefiksów)

var dz_verbs = [
"o/wy/do/od/po/u/z/za|bie @ dz",
"do/u/za/po|strze @ dz",
"po/dopo/wy/z/zanie|mó @ dz",
"wy/o/u/za|rze @ dz",
"xxxxy > xxxxy"
];

// poprawianie starych form słownego zapisu liczb

var count_array = [
"siedm > siedem",
"ośm > osiem",
"jedynas > jedenas",
"jedynaśc > jedenaśc",
"xxxxy > xxxxy"
];