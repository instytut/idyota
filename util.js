// Helper function: replace all occurences of a substring

String.prototype.repAll = function(_old, _new) {

  // in case string contains chars used in regular expressions:
  _old = _old.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');

  var reg = new RegExp(_old, "g");
  return this.replace(reg, _new);   
}

// does a string contain a substring?

String.prototype.contains = function(query) {

  if (this.indexOf(query) != -1) return 1;
  return 0;
}

// Find a html element with a given id 
// or complain that it's missing

function getNamedElement(name) {

  var el = document.getElementById(name);
  if (el == null) alert("HTML element " + name + " is missing!");
  return el;
}

function spanUnknown(word) {

  return "<span class='red'>"+ word + "</span>";
}

function isLowerCase(string) {
    return (string[0] === string[0].toLowerCase());
}

String.prototype.isAllCaps = function() {
    return this.valueOf().toUpperCase() === this.valueOf();
};

String.prototype.isAllLower = function() {
    return this.valueOf().toLowerCase() === this.valueOf();
};

function isUpperCase(string) {
    return (string[0] === string[0].toUpperCase());
}

// isSaneUpperCase() returns true if a word 
// starts  with capital letter and does not 
// contain any more capital letters.

function isSaneUpperCase(string) {

    var res = isUpperCase(string);
    if (res) {
         if (string.replace(/[^A-Z]/g, "").length > 1) 
             return false;
    }
    return res;
}

// capitalizeFirst() returns a word 
// with the capitalized first letter

function capitalizeFirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

// uncapitalizeFirst() returns a word 
// without the capitalized first letter

function uncapitalizeFirst(string) {
    return string.charAt(0).toLowerCase() + string.slice(1);
}

function getLast(arr) {
    return arr[arr.length - 1];
}

function wrapInBold(str) {
    return '<b>' + str + '</b>';
}

function wrapInBrackets(str) {
    return '(' + str + ')';
}

function download(filename, text) {

  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}