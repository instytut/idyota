function compressDictionary(inp, pre) {

    var out = [];
    var ln = inp.length;
    var pln = pre.length;
    inp.sort();

    for (var i = 0; i < ln; i++) {
        var cur = inp[i];
        if (cur.length <= pln
        ||  cur.contains("|"))
            out.push(cur);
        else {
            if (inDictionary(pre+cur, inp)) {
                // do nothing, unless double prefix
                if (cur.slice(0,pln) == pre ) {
                    var post = cur.slice(pln);
                    if (inDictionary(post, inp))
                       out.push(post);
                }
            } else if (cur.slice(0,pln) == pre ) {
                var post = cur.slice(pln);

                if (inDictionary(post, inp))
                    out.push(pre + "|" + post);
                else 
                    out.push(cur);
            } else {
                out.push(cur);
            }
        }
    }

    out.sort();
    saveDictionary("allwords", out);
}

function compressTwoStems(inp) {
    var out = [];
    var ln = inp.length;
    var block = false;

    inp.sort();	
	
    for (var i = 0; i < ln; i++) {

		if (inp[i].contains(':')
        &&  inp[i+1].contains(':')
	    &&  inp[i].isAllLower()
		&& inp[i+1].isAllLower()
	    &&  i < ln-2) {
			var div1 = inp[i].split(':');
			var div2 = inp[i+1].split(':');
			
			if (div1[0] == div2[0]) {
				block = true;
				out.push(div1[0]+':'+div1[1]+','+div2[1]);
			} else {
				if (!block) out.push(inp[i]);
				block = false;
			}
		} else {
				if (!block) out.push(inp[i]);
				block = false;
		}
	}
	
    out.sort();
    saveDictionary("allwords", out);
}

function sortDict(inp) {
	var out = inp.sort();
	saveDictionary("allwords", out);
}

function compressNullEnding(inp) {

    var out = [];
    var ln = inp.length;
    inp.sort();
    var block = false;
	
	for (var i = 0; i < ln-1; i++) {
		if (inp[i+1].contains(':') 
		&& inp[i+1].isAllLower() ) {
			var div = inp[i+1].split(':');
			if (div[0] == inp[i]) {
				block = true;
				var cur = div[0] + ':,' + div[1];
				out.push(cur);
			} else {
		      if (!block) out.push(inp[i]);
		      block = false;				
			}
			
		} else {
		  if (!block) out.push(inp[i]);
		  block = false;
		}
		
	}
	
	out.push("żżż");
    out.sort();
    saveDictionary("allwords", out);
}

function compressThird(inp) {
    var out = [];
    var ln = inp.length;
    inp.sort();
    var block = false;	
	
	// :na,ne,ć,ł,li" + liby
	
	for (var i = 0; i < ln; i++) {
		if (inp[i].contains(':na,ne,ć,ł,li')) {
           var div = inp[i].split(':');
		   if (inp[i+1] == div[0]+'liby') {
			   block = true;
			   out.push(div[0]+':na,ne,ć,ł,li,liby');
		   } else {
			   out.push(inp[i]);
		   }
		} else {
			if (!block) out.push(inp[i]);
			block = false;
		}
	}
	
    out.sort();
    saveDictionary("allwords", out);
}

function eliminateEnding(inp) {
    var out = [];
    var ln = inp.length;
    inp.sort();
    var block = false;	
	
	for (var i = 0; i < ln; i++) {
		if (inp[i].contains(':MYŻ')) {
           var div = inp[i].split(':');
		   if (inp[i+1] == div[0]+'że') {
			   block = true;
			   out.push(div[0]+':MYŻ');
		   } else {
			   out.push(inp[i]);
		   }
		} else {
			if (!block) out.push(inp[i]);
			block = false;
		}
	}
	
    out.sort();
    saveDictionary("allwords", out);
}

function compressTwoEndings(inp, e1, e2) {
	
    var out = [];
    var ln = inp.length;
    var eln = e1.length;
    inp.sort();
    var block = false;
	
   for (var i = 0; i < ln; i++) {
        if (inp[i].contains(":")
	    ||  inp[i].contains("+")) {
            block = false;
            out.push(inp[i]);
        } else {
		var root = inp[i].slice(0, inp[i].length-eln);
		if (root + e1 == inp[i]
		&&  root + e2 == inp[i+1]) {
                block = true;
                out.push(root+':'+e1+','+e2);
            } else {
                if (!block) out.push(inp[i]);
                block = false;
            }
        }
    }

    out.sort();
    saveDictionary("allwords", out);

}

function compressPostfix(inp, post) {

    var out = [];
    var ln = inp.length;
    var pln = post.length;
    inp.sort();
    var block = false;

    for (var i = 0; i < ln; i++) {
        if (inp[i].contains(":")) {
            block = false;
            out.push(inp[i]);
        } else {
            if (inp[i]+post == inp[i+1]) {
                block = true;
                out.push(inp[i]+'+'+post);
            } else {
                if (!block) out.push(inp[i]);
                block = false;
            }
        }
    }

    out.sort();
    saveDictionary("allwords", out);
}

function compressPrefixAtergo(inp, pre) {
	
	// NOTE: no sorting here
	
    var out = [];
    var ln = inp.length;
	var block = false;
	
	for (var i = 0; i < ln; i++) {
		if (inp[i+1] == pre + inp[i]
		&& !inp[i].contains('|')) {
			block = true;
		    out.push (pre + '|' + inp[i]);
		} else {
			if (!block) out.push(inp[i]);
			block = false;
		}
		
	}
	
	// Problem: returns sorted table
	
	saveDictionary("allwords", out);
	
}

function compareDict() {

  alert("porównanie");
    
  var ln = all.length;
  for (var i = 0; i < ln; i++) {
      var word = all[i];
      if (!inDictionary(word, _all)) 
      alert("w pierwszym słowniku brak " + word);
  }
  
  
  var ln = _all.length;
  for (var i = 0; i < ln; i++) {
      var word = _all[i];
      if (!inDictionary(word, all)) 
      alert("w drugim słowniku brak " + word);
  }
  
  alert("po porównaniu");
}

function decompressEnding (end) {
	
	end = end.replace('*NIA','nia,nie,niu');
	end = end.replace('*A,','a,ach,ami,');
	end = end.replace('*EK','ce,ek');
	end = end.replace('*ŚMY','ście,śmy');
	end = end.replace('*YMI','ym,ymi');
	end = end.replace('*ACH', 'ach,ami');
	end = end.replace('*OM', 'om,owi');
	end = end.replace('*Ą', 'ą,ę');
	end = end.replace('*EM', 'em,om');
	end = end.replace('*BY', 'by,bym,byś');
	end = end.replace('*Y', 'y,ych');
	end = end.replace('*EJ','ego,ej');
	end = end.replace('*MY','my,sz,cie');
	end = end.replace('*O','o,om');
	end = end.replace('*CH','ch,e,ej');
	
	
           if (end == "Y")       end = "a,e,ego,ej,i,y,ym,ymi,emu,ych";
    else if (end == "YM")    end = ",a,e,i,y,ym,ą";
	else if (end == "Ł")    end = "ć,ł,li";
	// good endings
	else if (end == "STA")    end = "sta,stach,stami,stą,stę,sty,stów,sto,stom";
    else if (end == "IA")    end = "ia,iach,iami,ią,ię,ii,ij,io,iom"; 
	else if (end == "SKI")    end = "skiego,skiemu";
	else if (end == "ARZ")    end = "arz,arza,arzowi,arzem,arzu,arze,arzy,arzami,arzach,arzom"; // dziennikarz, makaroniarz
	else if (end == "ARKA")    end = "arka,arki,arce,arkę,arką,arko,arek,arkom,arkami,arkach";  // dziennikarka, makaroniarka
	else if (end == "KA") end = "ce,ek,ka,kach,kami,ką,kę,ki,ko,kom";
	else if (end == "KI") end = "ka,ki,ko,ką,kim,kimi";
	else if (end == "CJA") end = "cja,cje,cją,cję,cji,cjo,cjom,cyj,cjach,cjami";
	else if (end == "TOR") end = "tor,tora,torach,torami,torem,torom,tory,torze,torzy,torów,torowi";
	else if (end == "CZYNA") end = "czyn,czyna,czynach,czynami,czyną,czynę,czyny,czynów,czynie,czyno,czynom";
	else if (end == "US") end = "us,usa,usach,usami,usom,usowi,usem,usie,usy,usów";
	else if (end == "METR") end = "metr,metrach,metrami,metrem,metrem,metrowi,metru,metry,metrze,metrów";
	else if (end == "ĄC") end = "ąc,ąca,ące,ącego,ącej,ącemu,ący,ącym,ącymi,ących,ącą";
	else if (end == "OWIEC") end = "owiec,owca,owcach,owcami,owce,owcem,owcom,owcu,owców,owcowi";
	else if (end == "ID") end = "id,idach,idami,idem,idom,idowi,idu,idy,idzie,idów";
	else if (end == "OWY") end = "owa,owe,owego,owej,owi,owo,owemu,owy,owym,owymi,owych,ową";
	else if (end == "CZEK") end = "czek,czka,czkach,czkami,czki,czkiem,czkom,czkowi,czku,czków";
	else if (end == "ER") end = "er,era,erach,erami,erem,erom,ery,erze,erowi,erów";
	else if (end == "UJ") end = "uj,ujże,ujemy,ujesz,ujmy,ujmyż,ują,uję,ujcie,uje";
	
	// TODO: metr
	else if (end == "NIU")    end =   ",na,ne,nej,ni,ny,ń,nym,ną,nia,nie,niu";
	else if (end == "NEGO")    end =   "nego,nej,nemu,nym,nymi";
	else if (end == "EJ")    end =    "ego,ej,emu,i,y,ym,ymi,ych";
	else if (end == "EJx")    end = ",ego,ej,emu,ym,ymi,ych";
	else if (end == "EJo")    end = "ego,ej,emu,ym,ymi,o,ych";
	else if (end == "EJą")    end = "ego,ej,emu,ych,ymi,ym,y,o,ą";
	else if (end == "IEM")    end = ",a,ach,ami,i,iem,om,owi,u,ów";
    else if (end == "CIE")    end = ",cie,cież,my,myż,ą,ę";
	else if (end == "CIEŻ")    end = ",cie,cież,my,myż,ą,ąc";
	else if (end == "ŻE")  end = ",cie,cież,my,myż,ą,że";
    else if (end == "CIEx")  end = ",cie,cież,my,myż,ą,ąc,e,ecie,emy,esz,ę,że";
    else if (end == "A-CH") end = "a,ach,ami,ą,ę,i,o";
	else if (end == "A-MI") end = "a,ach,ami";
	else if (end == "AMIx") end = ",a,ach,ami,e,ą,ę,om,y";
	else if (end == "ACHy") end = ",ach,ami,y,ów";
	else if (end == "ACH") end = ",ach,ami,em,om,y,ów,owi,u";
	else if (end == "ACHx") end = ",ach,ami,i,om,owi,u,ów";
	else if (end == "Ę") end = ",a,ach,ami,ą,ę,ie,o,om,y";
	else if (end == "Ęx") end = ",a,ach,ami,ą,ę,om,y";
	else if (end == "-ACH") end = "ach,ami,em,om"; 
	else if (end == "A") end = ",a,ach,ami,e,em,om";
	else if (end == "ÓW") end = ",a,ach,ami,ą,ę,y,ów,o,om";
	else if (end == "J") end = "a,ach,ami,e,ą,ę,i,j,o,om";
    else if (end == "EGO") end = "ego,ej,emu,y,ych,ym,ymi,i,ie";
	else if (end == "EGOx") end = "ego,emu,o,ych";
	else if (end == "EGOy") end = "ego,ej,i,y,ym,ymi,emu,ych";
	else if (end == "EMU") end = "ego,emu,ych,ymi";
    else if (end == "AŚ") end = "aś,eś,by,bym,byś,em,oby,y";
    else if (end == "ŚCIE") end = "ście,śmy";
	else if (end == "ŚCIEx") end = "byśmy,ście";
	else if (end == "ŚCIEb") end = ",ście,śmy,by";
    else if (end == "NA") end = "na,ne";
	else if (end == ",NA") end = ",na,ne";
	else if (end == "TA") end = "ta,te";
	else if (end == ",TA") end = ",ta,te";
	else if (end == "NI") end = "na,ne,nej,ni";
	else if (end == "ANO") end = "am,amy,ano,asz";
	else if (end == "WSZY") end = ",ą,ć,my,ono,sz,wszy";
	else if (end == "AI") end = "a,e,i,y,ą";
	
	else if (end == "cH") end = "ch,e,ego,ej,m,mi";
	else if (end == "CH") end = "ch,e,ego,ej,m,mi,emu";
	
	else if (end == "BY") end = ",by,bym,byś,m";
	else if (end == "-BY") end = "by,bym,byś,m";
	else if (end == "AŁ") end = "ał,ałby,ałbym,ałbyś,ałem,ałaś,ałeś,ało,ałoby,ałyby,ałyśmy, ałaby,ałabym,ałabyś,ałam";
	else if (end == "BYM") end = ",by,bym,byś,em,oby,y,aś,eś";
	else if (end == "BYŚ") end = "a,aby,by,bym,byś,em,oby,y,am,aś";
	else if (end == "CI") end = "ci,ciach,ciami,ciom,cią,ć";
	else if (end == "CIĄ") end = "ci,ciom,cią,ć";
	else if (end == "I") end = "a,i,o,ą";
	else if (end == "M") end = "m,mi";
	else if (end == "Ą") end = "a,e,y,ą";
	else if (end == "Ć") end = ",ć,ł";
	else if (end == "ŚĆ") end = "ści,ść";
	else if (end == "U") end = ",em,ie,y,ów,om,u";
	else if (end == "Y-M") end = "ej,y,ym,ą";
	else if (end == "AE") end = "a,e,ej,y,ym,ą";
	else if (end == "OM") end = "a,ach,ami,e,em,om";
	else if (end == "OMx") end = "a,e,em,om,u";
	else if (end == "AMI") end = ",a,ach,ami,e,ą,ę,o,om";
	else if (end == "EM") end = ",a,e,em,om";
	else if (end == "ECIE") end = ",e,ecie,emy,esz";
	else if (end == "E-MY") end = ",cie,cież,my,myż,ą,ę,e,ecie,emy,esz";
	else if (end == "OWI") end = "ach,ami,om,owi,y,ów";
	else if (end == "YCH") end = "a,e,ego,ej,emu,y,ym,ymi,ych,ą";
	else if (end == "MY") end = ",my,myż,ą,ąc";
	else if (end == "MYŻ") end = ",cie,cież,my,myż,że";
	else if (end == "IE") end = ",a,ach,ami,em,ie,o,om";
	else if (end == "EMx") end = ",ach,ami,om,owi,em,ie,y,ów";
	else if (end == "EMu") end = ",ach,ami,om,owi,em,ie,y,ów,u";
	else if (end == "ACHz") end = "ach,ami"; 
	else if (end == "AM") end = "am,aś,o,y";
	else if (end == "ĄĘ") end = "a,ach,ami,e,ą,ę,i,o,om";
	
	return end;
}

// extractDictionary()  takes  an  array
// of  words with prefixes, suffixes and endings,
//  and  creates  an  array of normal-looking words.

function extractDictionary(inp, out) {

    var ln = inp.length;

    for (var i = 0; i < ln; i++) {
        var item = inp[i];
        if (item.contains("|")) {
            var div = item.split("|");
			if (div[1].contains(":")) {
                var secDiv = div[1].split(":");
				secDiv[1] = decompressEnding(secDiv[1]);
                var altDiv = secDiv[1].split(",");
                var altCnt = altDiv.length;
				while (altCnt--) {
                    out.push(secDiv[0]+altDiv[altCnt]);
                    out.push(div[0]+secDiv[0]+altDiv[altCnt]);
                }
				
			} else if (div[1].contains("+")) {
                secDiv = div[1].split("+");
  
                // with ending, without prefix

                out.push(secDiv[0]);
                out.push(secDiv[0] + secDiv[1]);
                if (secDiv.length > 2) 
                    out.push(secDiv[0]+secDiv[1]+secDiv[2]);

                // with both ending and prefix
  
                out.push(div[0] + secDiv[0]);
                out.push(div[0] + secDiv[0] +secDiv[1]);
                if (secDiv.length > 2) 
                    out.push(div[0]+secDiv[0]+secDiv[1]+secDiv[2]);
                } else {
                    out.push(div[0]+div[1]);
                    out.push(div[1]);
                }
        } else if (item.contains("+")) {
            var secDiv = item.split("+");
            out.push(secDiv[0]);
            out.push(secDiv[0]+secDiv[1]);
            if (secDiv.length > 2) 
                out.push(secDiv[0]+secDiv[1]+secDiv[2]);
        } else if (item.contains(":")) { 
            var secDiv = item.split(":");
			secDiv[1] = decompressEnding(secDiv[1]);
            var altDiv = secDiv[1].split(",");
            var altCnt = altDiv.length;
            while (altCnt--) {
                out.push(secDiv[0] + altDiv[altCnt]);
            }
        } else {
           out.push(item);
        }
    }
}

// function saveDictionary() creates
// and  downloads a file  containing
// a  dictionary  table  in  form of
// a javaScript array;

function saveDictionary(name, arr) {

    var ln = arr.length;
    var outStr = 'var ' + name + ' = [ \n';

    for (var i = 0; i < ln; i++) {
        outStr += '"' + arr[i] + '",\n';
    }

    outStr += '];';
    download("out.txt", outStr);
}

// function inDictionary() checks whether
// a word exists in an array, using binary
// search.

function inDictionary(word, arr) {
   
    var lo  = 0;
    var hi  = arr.length - 1;
    if (hi < 1) return false;
    var mid = Math.floor((hi + lo)/2);
   
    while(arr[mid] != word && lo < hi) {
      if (word < arr[mid]) 
         hi = mid - 1;
      else if (word > arr[mid]) 
         lo = mid + 1;
      mid = Math.floor((hi + lo)/2);
    }

    if (arr[mid] == word) return true;
    return false;
}

function checkWord(curr) {

    if (curr == '[NEWLINE]') return true;

    // always accept one-letter words

    if (curr.length == 1) return true;

    if (curr.isAllCaps()) {
        if (inDictionary(curr, roman_num)) return true;
        curr = curr.toLowerCase();
    }

    if (isSaneUpperCase(curr)) {
       if (inDictionary(curr, names_array)) return true;
       curr = uncapitalizeFirst(curr);
    }

    if (inDictionary(curr, frequent))     return true;
    //if (inDictionary(curr, common))       return true;
    //if (inDictionary(curr, rare_array))   return true;
    if (inDictionary(curr, missed_array)) return true;

    // TODO: turn on upon finising dictionary compression
    if (inDictionary(curr, all))          return true;

    // foreign language dictionaries, just in case

    if (inDictionary(curr, franc_array)) return true;
    if (inDictionary(curr, ang_array))   return true;
    if (inDictionary(curr, niem_array))  return true;

    // tags

    if (inDictionary(curr, wl_array)) return true;

    return false;
}
 
function wordIsKnown(word) {

// DEPENDENCIES: array dontSplitByHyphen[] 
// is located in data.js

    if (word.contains('-')) {
       var parts = word.split('-');
       var ln = parts.length;
       while (ln--) {
          if (parts[ln] == '') return false;
          if (parts[ln] == undefined) return false;
          if (dontSplitOnHyphen.includes(parts[ln])) return false;
          if (!checkWord(parts[ln])) return false;
       }
          return true;
    }

    return checkWord(word);
}

function wordIsHint(curr) {

    if (inDictionary(curr, frequent)) return true;
    if (inDictionary(curr, hints)) return true;

    return false;
}
