— A jakoweż dokumenta mogą być? — Odparł Majster. — Chyba one szatki, co je miała na Leopolskim Rynku. Chcesz Waszmość, to choć to pokażę. Wołać mi tu Minę!
 I przywołanéj Minie rozkazał, aby przyniosła «kinderkowe» szatki.
 — Jest-ci — dorzuciła Hedwiga — i ten kochany Szkaplirzyk, aleć on nic nie powié.
 To mówiąc, z pod krézy wyciągnęła Szkaplerz mocno już znoszony.
 Pan Kaźmiérz pochwycił skwapliwie ten przedmiot, co przed chwilą spoczywał na jéj łonie, i ucałował go z podwojoném nabożeństwem.
 — I Wacpanna zawdy to nosisz?
 — O, zawdy! Już się sznurek przetarł, że ledwie dyszy, ale ja nie kładę nowego, bo to ten sam, uważ-że Wasza Miłość, ten sam co mi tam kładły Rodzice, pewnie dufający jako mię ta świętość zasalwuje.
 — Aha! I pięknie zasalwowała! — Podchwycił szyderczo Pan Schultz.
 — Aj, nie gadajcie tak Dobrodzieju. Zasalwowała, i jak jeszcze! Wszakcim ja nie ostała u Tatarów, jeno u Dobrodzieja chowam się po Chrześcyańsku.
 Tu pochyliła się do kolan Rajcy, a ten, głaszcząc ją po głowie, mówił:
 — Ej, ty ty łasico, ty zawdy umiész wszystko miodem posmarować.
 Widząc tę pieszczotę, chociaż zupełnie ojcowską, Pan Kaźmiérz zacisnął pięści pod stołem, i miał ochotę powiedzieć Panu Majstrowi coś

przykrego. Na szczęście, w téjże chwili powróciła Mina, niosąca piękną szkatułę z drewnianéj mozajki, o brązowych listwach i antabach.
 — Widzisz Waszmość, — rzekł gospodarz — jako to moja Dorotea miłowała naszą Hedvich, te nawet oto jéj gałganki chowała, niby jakie świętoście, w swoim najlepszym sepeciku.
 Tu podniósł wieko, i Pan Kaźmiérz zobaczył najprzód sukienkę z niebieskiego teletu, naszywaną drabinkami ze złotych passamonków, obrzeżoną u dołu i u góry sutém namarszczeniem ze złotych koronek, czyli jak wówczas nazywano «forbotów«. Sukienka była długa niby worek, ale maleńkie rozmiary staniczka, drobniutkie otwory na szyjkę i rączki, dowodziły że nosząca je dziecina, nie mogła miéć więcéj nad sześć do ośmiu miesięcy. Pod sukienką leżała koszulka, jeszcze drobniejsza, cała zahaftowana mnóstwem flamandzkich wszywek i obszywek, niegdyś białych, dziś od zleżenia mocno zżółkłych.
 Pan Kaźmiérz patrząc na te ubiorki, pomyślał sobie:
 — No, już teraz wiem dokumentnie, jako to nie jest Krysia, a to dla dwóch racyi. Pierwsza racya, że Krysia nigdyby w to nie była wlazła, kiedy już miała w one czasy kole czterech latek. Druga racya, że w naszym szaraczkowym domu, nigdy takowych luxusów nie znano. Pani Matka sama nie nosiła nijakich forbotów, ni białych ni złotych, a jeszcze by miała dzieciaki niemi pstrzyć? A toćby jéj Pan Ociec, Boże odpuść, był na

skórce forboty wydeseniował. Nie, to nie Krysia, i basta.
 Jednakże Pan Kaźmiérz nie zdradził się z żadném z tych spostrzeżeń, i ani zmrużył oczu gdy Pan Rajca wytrząsając sukienkę, mówił:
 — Zbrukana-ci ona, i nie dziw, przeszła bez łapy Tatarskie i bez Rynki Leopolskie, aleć zawdy widno jako to z pańskiego domu dziecko, z takiego, jak Waszmościny, nieprawdaż?
 Zagadnięty, wywinął się uwagą:
 — Ale jakożeś to Wacpanna robiła, aby się zmieścić w takowy łątkowy przyodziewek?
 A gdy Hedwiga zapytała:
 — Powiedz mi Waszmość, czy ta siostrzyczka była wonczas taka malutka jak i ja? A może Wasza Miłość sobie przypomnisz czy miała takowy przyodziewek?
 Odpowiedział wymijająco:
 — Juścić że mała była, to wiem. Ale czy miała takową sukienkę, tego ja wiedzieć nie mogę. Takie kuse chłopię jakom ja był wonczas, nie baczy na żadne białogłowskie ochędóztwa, jeno za batem i konikiem się ugania. Ale ja powiem Wacpaństwu tak: Jutro wracam doma. Tedy opowiem wszystko Panu Miecznikowi.....
 — Co to za Miecznik? — Spytał Majster.
 — A no, mój Ociec. Tedy powiem wiernie Panu Oćcu, a juści on będzie wiedział najlepiéj, czy Krysia miała takowe przyodzianie, i czy nosiła Szkaplirzyk, co jest u dziecka rzecz niebywała, i może stanąć za «lico». Tedy Wacpaństwu napiszę co Pan Ociec powiedział, a może

i sam przywiozę tę odpowieść. Nie miał-ci ja już co prawda tu powracać, ale dla Panny Siostry warto jachać i do morza, i nawet za morza.
 — Rozumnieś Wasza Miłość zakonkludował. Tedy Hedvich niémasz tu już co robić, odnieś ten sepecik do Miny, i ostań się tam w kuchni, a dopilnuj dla mnie kolacyi.
 Ale Pan Kaźmiérz okrzyknął się gwałtownie.
 — A nie rób-że mi Waszeć tak okrutnéj krzywdy. Wszakci ja wciąż gadam, jako jutro jadę, niechże dzisia jeszcze nacieszę się Panną Siostrą.
 Hedwiga także, składając ręce, mówiła z przymileniem:
 — Ach Dobrodzieju! Tam w kuchni takowy skwar! Jużem ja do kolacyi wszyściutko wydysponowała. Pozwól mi tu ostać.....
 Majster w końcu przystał, choć niechętnie.
 — Ha no, to już i ostań, jeno weź-że się do roboty, abyś nie siedziała jako ten szyld malowany, co to mówi: «A patrzajcież na mnie!»
 Panienka ucieszona, pobiegła do sieni, zkąd wtoczyła na ganek, leciuchny, misternie rzeźbiony kołowrotek, a Kornelius przyniósł dla niéj zydelek z białego jaworowego drzewa; śliczny to był sprzęcik; na trzech skośnych, kręconych nóżkach, miał deszczułkę z brzegiem wyrzynanym w liście jakby wianek, a z tyłu oparcie, wązkie w obsadzie, coraz szersze w górze, całe wzorzysto pokłute w przezrocza, niby wachlarz ze słoniowéj kości.

 Na téj filigranowéj podstawie, panienka przysiadła lekko jak ptaszę na gałązce, i błękitnym trzewiczkiem wprawiwszy w ruch warczące koło, zaczęła skręcać długie nitki lnu, blado-żółtego jak jéj włosy.



 Przy téj robocie tak cudnie wyglądała, że Pan Kaźmiérz zapatrzył się w nią całemi oczami, całą duszą, i zapamiętał się zupełnie.
 Już nie wiedział ani gdzie jest, ani kto na niego patrzy, tylko sam patrzył, patrzył, i w duszy sobie powtarzał:
 — Apparycyo ze złotą nitką, bądź-że Parką mojego żywota!



 Nagle przypomniał sobie gdzie i u kogo bawi, odkrząknął, zwrócił głowę do Pana Rajcy, i mówił:
 — Przypatrowam się bacznie Pannie Siostrze, bo chcę sobie zrememerować, do kogo z naszéj familii ona posiadła podobieństwo?
 Tu z kąta, głos Korneliusa odezwał się przekąśliwie:
 — Juści nie do Waszéj Miłości.
 Majster spojrzał w tamten kąt zdziwiony, chwilkę pomyślał, potém dobrodusznie się roześmiał, i mówił:
 — Ollender rzadko powié, ale już jak powié, to richtig. Waszmość — bez urazy — wyglądasz

jak kruk, a oko Waszmościne to jak ten Pharus co w ciemną noc nad portem dygota. No, a Hedvich, to bielusia jakbyja gniątko , a oczko jéj, Vergismeinnicht. Gdzież tu familia?
 — O, za pozwoleniem! — Zaprzeczył z wielką powagą Pan Kaźmiérz. - Abo to raz bywa rodzeństwo cale od siebie różne? A wszelako każde z nich podobne do kogóś z antecessorów. Tak i Panna Hedviga... Czekajcie Wacpaństwo, do kogo to ona podobna? A! Już wiem. Jakem był mały, przychadzała do nas jedna moja ciotka. Nie była-ci ona już taka młodziusia, ale włos miała żółtawy, i oczy jako dwa modre kwiatuszki. Owóż Panna Hedviga kubek w kubek do niéj podobniusieńka. O..... im więcéj patrzę, tém więcéj to dopatrowam.
 I na rachunek owéj żółto-włoséj ciotki, zaczął znowu, i coraz to już śmieléj, przyglądać się Pannie Siostrze.



 Ta, z początku rada była braterskiéj uwadze, przytém i podchlebiał jéj taki hołd milczący, bo dobrze czuła że w tych oczach pała nieopisany zachwyt. Niedługo jednak, rotowy ich ogień zaczął ją niepokoić i mieszać; po kilku chwilach, myślała że się skręci. Chcąc przerwać ten «urok» co stawał się cierpieniem, sama wróciła do rozmowy. Uśmiechnęła się i rzekła:
 — Ktoby to myślał, że my już raz tak bliziusio kole siebie byli, i może na się patrzali, i bez tyle lat nic o tém nie wiedzieli!

 — Kędyż to było? - Pytał zdziwiony Pan Kaźmiérz.
 — A no tam, na Leopolskim Rynku. Przypomnij sobie Waszmość dobrze, czyś tam nie widział wonczas takiéj małéj bzdury w niebieskiéj szatce i złotych forbotkach?
 — Wstydno to wyznać, aleć takowéj panienki nie pamiętam, acz pamiętam wiele innych rzeczy?
 — Na ten przykład, co?
 — Na ten przykład, kiedy nas wieźli, to pamiętam jako mię jeden Tatarzyn chciał na wozie związać, a jam się mu nie dawał, i takem się małemi rękami i nogami bronił, i takem się wściekał, że aż pomogło; bo właśnie tamtędy jachał sam ich wódz, on sławny Kantemir, a obaczywszy jako się dzieciak broni, zaczął się śmiać że aż się za boki brał, i cóś gadał do mego stróża, a ten zaraz dał mi pokój. Widno że mu się ten animusz w dziecku podobał, chciał sobie może ze mnie zrobić tęgiego Tatarzyna. Aleć ja leżąc na wozie, takem sobie medytował: «Kiedy się mnie sam wódz pogański przeląkł, to ja w nocy wstanę, i tego Kantemira i wszystkich jego Regimentarzy wyduszę, za co mię nasi zara Hetmanem okrzykną.»
 — A to piękna rzecz, kiedy chłopiec tak eroicznie roi. — Zauważyła z wysokiém zajęciem Hedwiga.
 — Byłby ja téż pewnie w nocy wylazł, i byliby mię jak szczenię ubili, jeno że właśnie tegoż dnia, jeszcze przed nocą, Pan Koniecpolski nadciągnął i wszystkich nas wyzwolił,

z czego ja nie był bardzo rad, bo już cały mój eroizm na nic się nie przydał. — Potém znowu pamiętam, że na onym Rynku, jakieś tłuste białogłowy posadziły mię na ziemi, pod szkarpą jakiegoś ogromnego domostwa, i kazały mi na głos wywoływać moją godność, aby mię familia łacniéj odszukała. Ale wnet pożałowały, bo jakem zaczął krzyczéć: «Jam jest Kazio Korycki! Kazio Korycki!» Tak na całym Rynku nikogo jenszego już nie było słychać jeno Kazia. Tedy mię znów prosiły abym przycichł, gębę mi łakociami zatykały, a ja nic, jeno się drę i drę, i takem się darł bez calutki dzień, aż mię i znalazło moje biédne matczysko, co już się téż wyrwało z pogańskich łyków.
 — Jakoż to? Więc i matka była w jassyr wzięta? No, a ta siostrzyczka, kiedyż ją z Waszmością rozłączono?
 — Od samego początku. Jeden porwał matkę, drugi siostrę, trzeci mnie, i wszyscy się rozlecieli na trzy strony, tak że ja z matką dopierom się tam na Rynku nalazł, a siostrzyczki już nikt nigdy nie widział, ani na pobojowisku, ani na woziech, ani w mieście. Nieraz my tak myśleli, że ją może Tatarzyn uciekając ubił, bo to nieraz oni wolą dziecko zarznąć, niżeli żywcem ostawić. A może ten co ją wiózł, potrafił uciec aż na Krym, boć i tacy byli co się przed nami salwowali. A może to po prostu jesteś Wacpanna?
 Tu Pan Majster, nalewając sobie po raz drugi kufel, otrząsnął się jak po zimnéj kąpieli.

— Brrr... Co to za szczęście że oni tu nie dojeżdżają! Paskudna rzecz taka wojna. Dobrześ Waszmość uczynił kiedyś wybrał służbę na wodzie.
 — A ja właśniem się już odprawił od Wodnéj Armaty.
 — O, szkoda! — Zawołała Hedwiga, i oczami zmierzywszy jego postać, mówiła:
 — Szkoda! Taki śliczny munderunek!
 — I ja powiedam: szkoda! Ha no, co robić, kiedy Pan Ociec każe?
 — Dla czego to?
 — Dla tego: było nas trzech braci.....
 — O? Niceś Waszmość nie mówił. To i oni poszli w on jassyr?
 — Nie. Oni jako starsze, już byli żakami u xięży we Lwowie. To ich salwowało. Tedy potém jeden poszedł pod chorągiew pancerną, a drugi do haltylyerów. Tedy ja widzę że już mi wzięli i ziemię i ogień, tedy proszę się Pana Oćca aby mi dał iść na wodę, bo to wonczas wszyscy gadali, co to będzie za flotta jakiéj świat nie widział, i że król buduje ono warowne miejsce Władysławów. Jam zawdy miłował awantury, i takem sobie myślał: «Niech jeno dostanę się na morze, będzie ich w bród.» Pan Ociec długo się wzbraniał, aż mię i odpuścił tutaj. Najprzód więc jachałem na okręciech kupieckich; było się tam i sam, widziało się świata kawał, ale awantur mało. Tedy przystałem do Wodnéj Armaty. A tu jeszcze gorzéj. O batalię nie łacno, jeśli się trafi to jak ślepéj kurze ziarno, a służba żmudna i sroga. Już ja tedy myślał wrócić pod

flagę kupiecką, i jachać do Indyów abo do Ameryki, za onym Panem Arciszewskim co tyle o nim gadania, aż tu przychodzi wiadomość, że starszy brat zginął w potrzebie, a w rok późniéj znowu wiadomość: drugi brat ubity, — a tu Pan Ociec pisze do mnie: «Wracaj-że mój ty Beniaminie, boś mi już jeden ostał się na świecie. Służ jako chcesz, jeno już na lądzie, abyś choć czasami przyjachał do mnie, i w gospodarce był mi sukkursem.» — Jakoż to nie posłuchać Oćca, staruszka samotnego? Wszystkich potracił, naprzód oną Krysię, potém Panią Matkę, potém dwóch starszych, już nikogo niéma jeno mnie.
 — I gdzież Waszmość się zaciągasz, do pancernych?
 — Owóż nie. Na wodzie człek odwykł od konia, lepiéj mi pójść do infanteryi. Hetman Zamoyski zawdy kawalerów tam namawiał, i gadał jako to teraz największa siła będzie w infanterzach. Tak to różnie ja się już przegryzował przez życie, i jako widzę jeszcze przyjdzie przegryzować się na inny manier, a zawdy awantur mało, chyba to może ja dzisia na nią się natknął, daj Boże szczęśliwie.



 W téj chwili Pan Majster, który już sobie nalewał trzeci kufel piwa, chciał i gościowi napełnić szklenicę, ale spostrzegł że jest ledwie troszkę nadpoczęta.

 — O! — Zawołał ze zgorszeniem. — Jaki to Waszmość niełaskaw na mój bier. A paradny bier, keine gadanie paradny. Czy nieprawda?
 Pan Kaźmiérz zakłopotany odpowiedział:
 — A no tak... dobry... jeno my na wodzie skąpo zażywamy piwa... to człek i nie przywykł.
 Gdy kończył te słowa, Hedwiga się zerwała jakby wicher, i wpadłszy do sieni, drzwi za sobą zaparła.
 — Co się stało? — Zapytał Pan Kaźmiérz.
 — A kto ją wié? — Odrzekł Majster. — Ona nigdy nie może na miejscu spokojnie usiedziéć. A i ty Kornelius, czego tak stoisz jak ten słup co drzwi podpiera? Siadaj-że na swojém «ruhe.» — Trzeba wiedziéć Waszéj Miłości, jako my tu mamy nasze miejsca na zawdy nominowane. Ja siaduję tu pod złotém oknem, i ten kąt zwie się: «Faterstuhl.» Moja Dorotea siadywała tuż kole drzwi, aby łacno mogła dopaść kuchni, to my zwali jéj ławę «na tymczasem.» Hedvich na swoim zydelku, my gadamy «na skrzydełku,» bo wiecznie lata. No, a tam po drugiéj stronie drzwiów, ten drugi kąt ławy to «Kornelius-ruhe,» bo Kornelius tam nic nie gada, jeno siedzi jak mruk, i wzdycha za swoim Amsterdamem. A grzeszy chłopak; dziękowałby Panu Bogu że jest w mieście tak zacném i sławném jako nasz Dantzig. Gadaj-no Kornelius, u was tam niéma takich złoconych i rzezanych domów, ani takich beischlagów, co?
 — Prawda Panie Majster, beischlagów niéma, aleć za to u nas w Amsterdamie niech jeno człowiek wsadzi w okno głowę, a już cały dycha

słonością wiatrów morskich, a tu co? Ani wié że jest kole morza.
 — Gadaj co chcesz, już ja tam wolę nie miéć słoności w gębie, a dychać naszą aurą co jest mocna i słodka jakoby Goldwasser. On mi się zawdy chwali ze swemi Amsterdamami, Rotterdamami, Harlemami, a ja powiedam: pierwsze miasto w świecie, Dantzig. Czy nieprawda, powiedz sam Waszmość?
 Kaźmiérz się uśmiechnął.
 — Żeby nie Kraków, tobym dał Waszeci racyę.
 — Wasza Miłość nie byłeś w Amsterdamie? — Zapytał Kornelius.
 — A byłem. Gdzieżem-ci ja nie był? Ze mnie wielki nawigant. Jeno powiedam: «Wszędzie dobrze, ale w domu najlepiéj.» Z téj racyi téż i Waści panie Czeladniku nie ganię że chwalisz swoje Amsterdamy. Każda liszka swój ogon chwali. Niechże Amsterdam będzie ogonem wszelakich miast świata.
 Kornelius, który słabo jeszcze podchwytywał odcienia polskiéj mowy, zamyślił się, niewiedząc czy ma na te słowa odpowiedziéć uśmiechem czy gniewem? W każdym razie, czuł większą skłonność do gniéwu. Ten gość, choć prawie nieznajomy, był mu od pierwszéj chwili niemiłym. Istnieją w ludzkich sercach przeczucia.

Gdy tak sobie nieznacznie docinali, drzwi domu się rozwarły, i Hedwiga powróciła, niosąc znowu tacę, nierównie mniejszą niż pierwsza, ale stokroć piękniejszą, całą wyzłocistą. Na tacce stały trzy naczynia, istne pieścidełka sztuki. Nalewka złota w kształcie ptaka, cała emaliami różnobarwnemi pokryta, jakby najwytworniejsza kraska. Przy niéj stał kielich ze szkła jasno-amarantowego, wyglądający na olbrzymi, wyżłobiony rubin; po jednéj stronie był na nim wymalowany król ptaków, z rozpostartemi skrzydłami, z koroną u głowy; po drugiéj stronie był napis polski. «Na zdrowye!» Między kielichem i nalewką, stała na złotéj nóżce, mieniąca się stu kolorami opalu, wydrążysta koncha, pełna malutkich marcepanów. Pan Kaźmiérz, ze zwykłą zakochanym bystrością, zauważył od razu że wszystkie miały kształt serduszek.
 Pan Majster spojrzał na Hedwige z podziwieniem, i zapytał półgłosem:
 — Kto ci kazał to wszystko tu windować?
 Panienka zmieszana odparła:
 — Mnie się uwidziało..... że sam Dobrodziéj chciał..... Jego Miłości nie smakuje nasze piwko, i pierniczka ledwie nadkąsił, więc dałam petercymentu..... no, i tych Królewieckich marcypanków..... Może będą lepiéj do gustu?
 — A będą, boś Wacpanna wybrała nic jeno serduszka.
 Hedwiga zapłoniła się po same oczy.
 — Wszakci ja mam być siostra, to winnam Waszmości dać serce?

— Biorę tedy serduszko Wacpanny.
 To mówiąc, włożył do ust marcepanik, i smakując w nim z rozkoszą, powtarzał:
 — Aj! Co za specyalik! Do śmierci ta słodycz mi ostanie.
 Hedwiga tymczasem pochyliwszy kraskę, ze złotego jéj dzióba lała napój gęsty i ciemny, tak że kielich z amarantowego stał się morderowym.
 Potém powróciła do kołowrotka. Robota jednak szła niesporo, kółko co chwila stawało, prządka wolała zasłuchiwać się w rozmowie gościa.
 Ten, ująwszy kielich, wniósł «Zdrowie Panny - Siostry,» pokosztował, i mówił:
 — Ono zdrowie chciałbym duszkiem wypić, aleć — jako widzę, — nie godzi się takowego nektaru łykać bez konsyderacyi. Wyborność.
 I powoli popijał, a pijąc przyglądał się ciekawie naczyniom.
 — Co téż to u Waszeci precyozów!
 — A, mamci tego trocha, bom zawdy adorował wszelakie meister-sticki. Ten kielich to pamięta króla Zygmunta Augusta. Nalewka przyszła mi z miasta Italskiego Firency, gdzie są wielkie Meistry złotniki. A ta skorupa na konfekty, — patrz jeno Waszmość co tu kolorów lata w oczach, i purpura, i lazur, i złote płatki, — to jest, morski rarytas, Nautylus.
 — Przebóg! Nautylus, o którym jam się tyle naśpiéwał! — Zawołał Pan Kaźmiérz. — A dajże go tu Panie Konzulu, niechże mu się naprzypatrowam. Jam go zawdy był taki ciekaw, żem już

nieraz chciał jachać na morza Greckie, albo Indyańskie, aby tę bestyę obaczyć.
 — A co to Waszmość śpiewasz o tym ślimaku? — Pytała Hedwiga.
 — To, co IMci Pan Klonowicz o nim pisze w swoim «Flisie.» Znasz Wacpan tę xiążkę? Nie? A Wacpanna?
 — I ja nieznam, jeno Pani Flora mi gadała nieraz, o takowéj pieśni co ma być «Flis.» I ucieszne to?
 — Powiem Wacpannie, cud świata. Jam zawsze kochał się w poethusach. A już na wodzie, to najlepsza konsolacya. Bo to na okręciech nudno, ni podwiki ni pląsów, dzień jeden jako i drugi. Jak człek dorwie się niektórego voluminu, to i dziesięć razy w kółko czyta. Owóż ja tak onego «Flisa» zwertowałem, co go prawie całkiego mam w głowie. Szkoda że Wacpaństwo nie czytali, tam jest i o Gdańsku. Jeden z naszych Officyjerów przywiózł tę xiążczynę z Warszawy, to nieraz bywało wieczorami, siedzimy na pokładzie, a szczęki nam od ziewania trzeszczą, alić jak on zacznie nam one wiersze dyktować, a my wszyscy chorusem powtarzamy, tak i wieczór zleci jakoby z bicza trzasł.
 — I co on tam — pytała Hedwiga — może pisać o takowym mizernym robaczku?
 — Chcesz Wacpanna, to zaśpiéwam?
 — A mój Boże! Toż dopiero będzie nam uciecha!
 — A może na ulicy nie uchodzi?
